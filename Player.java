import java.io.*;
public abstract class Player {
    protected String name;
    protected char mark;
    protected Board board;
    protected Player opponent;
    protected boolean isPlaying;

    public Player(String name,char mark){
        this.name= name;
        this.mark = mark;
    }
    public void printName(){
            System.out.println("The "+this.mark+"player :"+this.name+" has won");
    }

    public void play(MyFrame frame) {
        if (/*!board.xWins() && !board.oWins() &&*/ !board.isFull()) {
            this.makeMove(frame);
            //board.display();
        }

       /* if (board.oWins() || board.xWins()) {
            System.out.println("Game Ended");
            if (board.xWins()) {
                System.out.println("The X-Player won");

            } else if (board.oWins()) {
                System.out.println("The O-Player won");
            }
            printName();
        } else if (board.isFull()) {
            System.out.println("The game is a tie");

        }*/

    }

    public abstract void makeMove(MyFrame frame);


    /*Getters and Setters*/
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public char getMark() {
        return mark;
    }

    public void setMark(char mark) {
        this.mark = mark;
    }

    public Board getBoard() {
        return board;
    }

    public void setBoard(Board board) {
        this.board = board;
    }

    public Player getOpponent() {
        return opponent;
    }

    public void setOpponent(Player opponent) {
        this.opponent = opponent;
    }

}
