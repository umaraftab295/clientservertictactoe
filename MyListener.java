import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import javax.swing.*;



class MyListener implements ActionListener,Constants {
    private MyFrame frame;
    Referee theRef;
    Player xPlayer, oPlayer;
    GameLab3 game;

    public MyListener(MyFrame jf) {
        frame = jf;
        game= new GameLab3();
        theRef = new Referee( );
        theRef.setBoard(game.getTheBoard());

    }

    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == frame.player1Submit){
            frame.comp.append(frame.player1.getText()+" is Player X\n");
            xPlayer = game.create_player (frame.player1.getText(), LETTER_X, game.getTheBoard());
            theRef.setxPlayer(xPlayer);
            frame.player1Submit.setEnabled(false);
        }
        if(e.getSource() == frame.player2Submit){
            frame.comp.append(frame.player2.getText()+" is Player O\n");
            oPlayer = game.create_player(frame.player2.getText(),LETTER_O,game.getTheBoard());
            theRef.setoPlayer(oPlayer);
            frame.player2Submit.setEnabled(false);
        }
        if(e.getSource() == frame.startGame){
            frame.startGame.setEnabled(false);
            game.appointReferee(theRef,frame);
        }


    }
}