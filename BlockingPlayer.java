public class BlockingPlayer extends RandomPlayer {

    //private String name;
    //private char mark;

    public BlockingPlayer(String name,char mark){
        super(name,mark);
    }

    public boolean testForBlocking(int row, int col){
        board.addMark(row,col,super.mark);
        /*if(board.checkWinner(super.mark) == 1){
            return true;
        }*/
        return false;

    }

    public void makeMove(MyFrame frame){
        for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
                board.removeMark(i,j);
                if(this.testForBlocking(i,j)){

                    //board.addMark(i,j,super.mark);
                    break;
                }else{
                    super.makeMove(frame);
                }
            }
        }
    }
}
