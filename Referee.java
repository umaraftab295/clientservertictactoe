import javax.swing.*;
import java.awt.event.ActionListener;

public class Referee {
    private Player xPlayer;
    private Player oPlayer;
    private Board board;


    public Referee(){

    }

    public void runTheGame(MyFrame frame){
        this.board = frame.board;
        xPlayer.setOpponent(this.oPlayer);
        oPlayer.setOpponent(this.xPlayer);
        board.setPlayerX(this.xPlayer);
        board.setPlayerO(this.oPlayer);
        char returnMark, nextreturnMark;
        System.out.println(frame.player1.getText());
        System.out.println("The referee started the game...");
        //board.clear();
        frame.comp.append("The referee started the game\n");
        frame.revalidate();
        board.displayButtonsEnable(frame);
        board.setReferee(this);
        gamePlay(frame);




/*
        if(!board.xWins() && !board.oWins() && !board.isFull()) {
            //board.display();
            while (!board.oWins() || !board.xWins() || !board.isFull()) {
                returnMark = xPlayer.getMark();
                if (returnMark == 'X') {
                    oPlayer.play(frame);
                    nextreturnMark = oPlayer.getMark();
                    if (nextreturnMark == 'O') {
                        xPlayer.play(frame);
                    }
                }
                if(board.oWins() || board.xWins() || board.isFull()) {
                     break;
*//*                  System.out.println("Game Ended");
                    if (board.xWins()) {
                        System.out.println("The X-Player won" + xPlayer.getName());

                    } else if (board.oWins()) {
                        System.out.println("The O-Player won" + oPlayer.getName());
*//*
                }
            }
        }*/
    }

    public void gamePlay(MyFrame frame){
        if (frame.board.getMark() == 'X') {
            oPlayer.play(frame);
            oPlayer.isPlaying = true;
            xPlayer.isPlaying = false;
        } else if (frame.board.getMark() == 'O' || frame.board.getMark() == 'A') {
            xPlayer.play(frame);
            xPlayer.isPlaying = true;
            oPlayer.isPlaying = false;
        }
    }

    public Player getxPlayer() {
        return xPlayer;
    }

    public void setxPlayer(Player xPlayer) {
        this.xPlayer = xPlayer;
    }

    public Player getoPlayer() {
        return oPlayer;
    }

    public void setoPlayer(Player oPlayer) {
        this.oPlayer = oPlayer;
    }

    public Board getBoard() {
        return board;
    }

    public void setBoard(Board board) {
        this.board = board;
    }

}
