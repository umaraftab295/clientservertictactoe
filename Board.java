import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

//STUDENTS SHOULD ADD CLASS COMMENTS, METHOD COMMENTS, FIELD COMMENTS
public class Board extends JPanel implements Constants {
	private char theBoard[][];
	protected int markCount;
	protected JButton[] buttons = new JButton[9];
	protected Player playerX;
	protected Player playerO;
	protected char mark;
	protected Referee referee;

	public Referee getReferee() {
		return referee;
	}

	public void setReferee(Referee referee) {
		this.referee = referee;
	}



	public char getMark() {
		return mark;
	}

	public void setMark(char mark) {
		this.mark = mark;
	}



	public Player getPlayerX() {
		return playerX;
	}

	public void setPlayerX(Player player1) {
		this.playerX = player1;
	}

	public Player getPlayerO() {
		return playerO;
	}

	public void setPlayerO(Player player2) {
		this.playerO = player2;
	}
	public Board() {
		setLayout(new GridLayout(3,3));
		displayButtons();
		markCount = 0;
		mark='A';
		/*theBoard = new char[3][];
		for (int i = 0; i < 3; i++) {
			theBoard[i] = new char[3];
			for (int j = 0; j < 3; j++)
				theBoard[i][j] = SPACE_CHAR;
		}*/
	}

	public char getMark(int row, int col) {
		return theBoard[row][col];
	}

	public boolean isFull() {
		return markCount == 9;
	}

	public char whoIsPlaying(Player p){
		if(p.isPlaying == true){
			if(p.getMark() == LETTER_X)
				return LETTER_X;
			else
				return LETTER_O;
		}
		return 'A';
	}

/*	public boolean xWins() {
		if (checkWinner(LETTER_X) == 1)
			return true;
		else
			return false;
	}

	public boolean oWins() {
		if (checkWinner(LETTER_O) == 1)
			return true;
		else
			return false;
	}*/

	/*public void display() {
		displayColumnHeaders();
		addHyphens();
		for (int row = 0; row < 3; row++) {
			addSpaces();
			System.out.print("    row " + row + ' ');
			for (int col = 0; col < 3; col++)
				System.out.print("|  " + getMark(row, col) + "  ");
			System.out.println("|");
			addSpaces();
			addHyphens();
		}
	}*/

	public void displayButtons() {
		//displayColumnHeaders();
		//addHyphens();

		for (int i = 0; i < 9; i++) {
			buttons[i] = new JButton();
			buttons[i].setText(" ");
			buttons[i].setFont(new Font("Arial", Font.PLAIN, 60));
			buttons[i].setPreferredSize(new Dimension(100,100));
			buttons[i].setEnabled(false);
			add(buttons[i]);
		}
	}
	public void displayButtonsEnable(MyFrame frame){
		for(int i=0;i<9;i++){
			buttons[i].setEnabled(true);
			buttons[i].addActionListener(new ButtonListener(frame,playerX,playerO));
		}
	}

	public void addMark(int row, int col, char mark) {
		theBoard[row][col] = mark;
		markCount++;
	}
	public void addMarkButton(int row, int col,char mark){

	}

	// Added for Blocking Player
	public void removeMark(int row, int col){
		theBoard[row][col] = SPACE_CHAR;
		markCount--;
	}

	/*public void clear() {
		for (int i = 0; i < 3; i++)
			for (int j = 0; j < 3; j++)
				theBoard[i][j] = SPACE_CHAR;
		markCount = 0;
	}*/
	public void clearButtons(){
		for(int i=0;i<9;i++){
			buttons[i].setText(" ");
		}
	}

/*	int checkWinner(char mark) {
		int row, col;
		int result = 0;

		for (row = 0; result == 0 && row < 3; row++) {
			int row_result = 1;
			for (col = 0; row_result == 1 && col < 3; col++)
				if (theBoard[row][col] != mark)
					row_result = 0;
			if (row_result != 0)
				result = 1;
		}

		
		for (col = 0; result == 0 && col < 3; col++) {
			int col_result = 1;
			for (row = 0; col_result != 0 && row < 3; row++)
				if (theBoard[row][col] != mark)
					col_result = 0;
			if (col_result != 0)
				result = 1;
		}

		if (result == 0) {
			int diag1Result = 1;
			for (row = 0; diag1Result != 0 && row < 3; row++)
				if (theBoard[row][row] != mark)
					diag1Result = 0;
			if (diag1Result != 0)
				result = 1;
		}
		if (result == 0) {
			int diag2Result = 1;
			for (row = 0; diag2Result != 0 && row < 3; row++)
				if (theBoard[row][3 - 1 - row] != mark)
					diag2Result = 0;
			if (diag2Result != 0)
				result = 1;
		}
		return result;
	}

	void displayColumnHeaders() {
		System.out.print("          ");
		for (int j = 0; j < 3; j++)
			System.out.print("|col " + j);
		System.out.println();
	}

	void addHyphens() {
		System.out.print("          ");
		for (int j = 0; j < 3; j++)
			System.out.print("+-----");
		System.out.println("+");
	}

	void addSpaces() {
		System.out.print("          ");
		for (int j = 0; j < 3; j++)
			System.out.print("|     ");
		System.out.println("|");
	}*/

	public boolean checkForWin()
	{
		/**   Reference: the button array is arranged like this as the board
		 *      0 | 1 | 2
		 *      3 | 4 | 5
		 *      6 | 7 | 8
		 */
		//horizontal win check
		if( checkAdjacent(0,1) && checkAdjacent(1,2) ) //no need to put " == true" because the default check is for true
			return true;
		else if( checkAdjacent(3,4) && checkAdjacent(4,5) )
			return true;
		else if ( checkAdjacent(6,7) && checkAdjacent(7,8))
			return true;

			//vertical win check
		else if ( checkAdjacent(0,3) && checkAdjacent(3,6))
			return true;
		else if ( checkAdjacent(1,4) && checkAdjacent(4,7))
			return true;
		else if ( checkAdjacent(2,5) && checkAdjacent(5,8))
			return true;

			//diagonal win check
		else if ( checkAdjacent(0,4) && checkAdjacent(4,8))
			return true;
		else if ( checkAdjacent(2,4) && checkAdjacent(4,6))
			return true;
		else
			return false;


	}

	public boolean checkAdjacent(int a, int b)
	{
		if ( buttons[a].getText().equals(buttons[b].getText()) && !buttons[a].getText().equals(" ") )
			return true;
		else
			return false;
	}

}
