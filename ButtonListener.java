import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ButtonListener implements ActionListener {
    private MyFrame frame;
    private Player xPlayer;
    private Player oPlayer;



    public ButtonListener(MyFrame f,Player x,Player o){
        frame=f;
        xPlayer = x;
        oPlayer = o;
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        JButton buttonClicked = (JButton)e.getSource();
        if(xPlayer.isPlaying == true){
            buttonClicked.setText("X");
            frame.board.setMark('X');



        }else if(oPlayer.isPlaying == true){
            buttonClicked.setText("O");
            frame.board.setMark('O');
        }
        frame.board.markCount++;
        frame.board.referee.gamePlay(frame);


        if (frame.board.checkForWin() || frame.board.isFull()) {
            //board.display();
             JOptionPane.showMessageDialog(null,"THATS THE END OF THE GAME");
             if(frame.board.checkForWin() && frame.board.mark == 'X'){
                 JOptionPane.showMessageDialog(null,xPlayer.name+ "won the game");
             }else if(frame.board.checkForWin() && frame.board.mark == 'O'){
                 JOptionPane.showMessageDialog(null,oPlayer.name+ "won the game");
             }else if(frame.board.isFull()){
                 JOptionPane.showMessageDialog(null," It was a tie");
             }

             frame.board.clearButtons();
             frame.comp.setText("Please enter the name of the 'X' player: in XPlayer box\n" +
                     "Please enter the name of the 'O' player: in OPlayer box\n");
             frame.player1Submit.setEnabled(true);
             frame.player2Submit.setEnabled(true);
             frame.startGame.setEnabled(true);
             frame.board.setMark('A');
             frame.board.markCount = 0;
        }


    }
}