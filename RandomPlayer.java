import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class RandomPlayer extends Player {
    //private String name;
    //private char mark;
    RandomGenerator rGen;

    public RandomPlayer(String name, char mark) {
        super(name, mark);
    }


    public void makeMove(MyFrame frame) {
        rGen = new RandomGenerator();


        while(true) {
            int row = rGen.discrete(0,2);
            int col = rGen.discrete(0,2);
            if (board.getMark(row, col) == Constants.SPACE_CHAR) {
                board.addMark(row, col, super.mark);
                break;
            }else if(board.getMark(row,col) == Constants.LETTER_X || board.getMark(row,col) == Constants.LETTER_O){
               rGen = new RandomGenerator();
            }
        }

        //board.addMark(rowI, colI, this.mark);
    }
}