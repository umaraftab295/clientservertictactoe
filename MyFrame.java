import javax.swing.*;
import java.awt.*;


public class MyFrame extends JFrame {
    //JButton insertB, findB,browseB,createB;
    MyListener listener;
    JTextArea comp;
    JTextField player1,player2;
    JLabel player1Label,player2Label,statsComp;
    JPanel panelButton,panelText,panelFields;
    JButton player1Submit,player2Submit,startGame;
    Board board;
    GridBagConstraints cGrid;
    public MyFrame(String s){
        super(s);
        listener = new MyListener(this);
        board = new Board();
        player1Label = new JLabel("XPlayer Name");
        player2Label = new JLabel("OPlayer Name");
        player1 = new JTextField();
        player2 = new JTextField();
        player1Submit = new JButton("Submit XPlayer");
        player2Submit = new JButton("Submit OPlayer");
        startGame = new JButton("Start Game");
        panelButton= new JPanel (new GridBagLayout());
        panelText = new JPanel(new GridBagLayout());
        panelFields = new JPanel(new GridBagLayout());
        statsComp = new JLabel();
        comp=new JTextArea(15, 20);
        statsComp.setText("Game Proceedings");

        /*Add action listener*/
        player1Submit.addActionListener(listener);
        player2Submit.addActionListener(listener);
        startGame.addActionListener(listener);

        cGrid= new GridBagConstraints();

        cGrid.insets = new Insets(20,20,20,20);
        cGrid.gridx=0;
        cGrid.gridy=0;
        panelButton.add(board,cGrid);


        cGrid.gridx=0;
        cGrid.gridy=0;
        panelText.add(statsComp,cGrid);

        cGrid.gridx=0;
        cGrid.gridy=1;
        panelText.add(comp,cGrid);

        cGrid.gridx=0;
        cGrid.gridy=0;
        panelFields.add(player1Label,cGrid);
        cGrid.gridx=0;
        cGrid.gridy=1;
        panelFields.add(player1,cGrid);
        cGrid.gridx=0;
        cGrid.gridy=2;
        panelFields.add(player1Submit,cGrid);
        player1.setPreferredSize(new Dimension(150,30));

        cGrid.gridx=1;
        cGrid.gridy=0;
        panelFields.add(player2Label,cGrid);
        cGrid.gridx=1;
        cGrid.gridy=1;
        panelFields.add(player2,cGrid);
        cGrid.gridx=1;
        cGrid.gridy=2;
        panelFields.add(player2Submit,cGrid);
        player2.setPreferredSize(new Dimension(150,30));

        cGrid.gridx=2;
        cGrid.gridy=2;
        panelFields.add(startGame,cGrid);

        /* Panels added to frame*/
        add(panelText,BorderLayout.EAST);
        add(panelButton,BorderLayout.CENTER);
        add(panelFields,BorderLayout.NORTH);
    }


    public JTextArea getComp() {
        return comp;
    }

    public void setComp(JTextArea comp) {
        this.comp = comp;
    }

    public JTextField getPlayer1() {
        return player1;
    }

    public void setPlayer1(JTextField player1) {
        this.player1 = player1;
    }

    public JTextField getPlayer2() {
        return player2;
    }

    public void setPlayer2(JTextField player2) {
        this.player2 = player2;
    }

    public JPanel getPanelButton() {
        return panelButton;
    }

    public void setPanelButton(JPanel panelButton) {
        this.panelButton = panelButton;
    }

    public Board getBoard() {
        return board;
    }

    public void setBoard(Board board) {
        this.board = board;
    }

}
